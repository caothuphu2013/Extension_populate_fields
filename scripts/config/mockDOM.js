const config = {
    DOM: {
        passwordDOM: "input[type='password']",
        usernameDOM: "input[type='text'] , input[type='username'], input[type='email'], input:not([type])",
        submitDOM: "input[type='submit'] , input[type='image'], button[type='submit'], button:not([type])",
        formDOM: "form"
    }
}