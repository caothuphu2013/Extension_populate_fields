var configObject = {
    logType: {
        error: 'error',
        info: 'info',
        warning: 'warning',
        debug: 'debug'
    },
    path: {
        extensionPath: '/scripts/extension.js'
    },
    domainURL: 'tiki.vn',
    apiDomainURL: 'https://5b8e3eb55722ac00143174fd.mockapi.io/api/v1'
};
