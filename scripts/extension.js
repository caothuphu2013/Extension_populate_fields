/**
 * Global variables:
 * - configObject
 */
(function($){
    'use strict';

    var utility = new Utility();
    var debug = new Debug();

    var apiCaller = new ApiCaller();
    var formDiscovery = new FormDiscovery();

    $(document).ready(function(){
        debug.log('Bootstrap extension.js sucessfully!');
        setupContentScriptData();
        initContentScript();
    });

    function setupContentScriptData() {
        formDiscovery.init(utility);
    }

    function initContentScript() {
        chrome.extension.onMessage.addListener(function(request) {
            // debug.log('request', request, configObject.logType.debug);
            switch(request.message) {
                case 'requestPageInfo':
                    debug.log('requestPageInfo', null, configObject.logType.debug);
                    getPageCredential();
                    break;

                case 'launchFormDiscovery':
                    debug.log('launchFormDiscovery', null, configObject.logType.debug);
                    launchFormDiscovery();
                    break;
                case 'populateCredential':
                    debug.log('populateCredential', request.data, configObject.logType.debug);
                    // getPageCredential();
                    populateCredential(request.data.formFields, request.data.credential);
                    break;

                default: break;
            }
        });

        // Inform for background.js that content script is injected and is waiting for next action
        chrome.runtime.sendMessage({ message: 'evaluateVault' });
    }

    function getPageCredential() {
        $.when(apiCaller.getCredential('/credencial/1')).then(function(credential) {
            // If this page is supported and has credential
            if(credential) {
                chrome.runtime.sendMessage({
                    message: 'createNewSingleVault',
                    data: {
                        credential: credential,
                        // formFields: formFields
                    }
                });
            }
        }, function() {
            // If this page isn't supported
            debug.log('Page is not support');
        });
    }

    function launchFormDiscovery() {
        $.when(formDiscovery.launchFormDiscovery()).then(function(formFields) {
            chrome.runtime.sendMessage({
                message: 'updateFormFields',
                data: {
                    formFields: formFields
                }
            });
        }, function(error) {
            debug.log(error);
        });
    }

    function populateCredential(formFields, credential) {
        $.when(formDiscovery.populateCredential(formFields, credential)).then(function(){
            chrome.runtime.sendMessage({
                message: 'endProcess'
            })
        }, function(error) {
            debug.log(error)
        });
    }

})($);
