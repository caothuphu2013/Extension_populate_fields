/**
 * Global variables:
 * - configObject
 */
function ApiCaller() {

    "use strict";

    var _header = {
        'content-type': 'application/json'
    };

    this.getCredential = function(api) {
        return $.Deferred(function (dfd) {
            var request = _get(api);

            request.done(function(response) {
                dfd.resolve(response);
            });

            request.fail(function(response) {
                dfd.reject(response);
            });
        }).promise();
    };

    function _get(api, queryParams) {
        return $.ajax({
            method: 'GET',
            url: configObject.apiDomainURL + api,
            header: _header,
            data: queryParams
        });
    }

    function _post(api, data) {
        return $.ajax({
            method: 'POST',
            url: configObject.apiDomainURL + api,
            header: _header,
            data: data
        });
    }
}
