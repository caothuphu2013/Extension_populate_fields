function Debug() {

    'use strict';

    this.log = function(message, data, logType) {
        if(!message) message = '';
        if(!data) data = '';

        switch(logType) {
            case 'error':
                console.error('[ERROR] ', message, data); break;
            case 'info':
                console.info('[INFO] ', message, data); break;
            case 'warning':
                console.warn('[WARN] ', message, data); break;
            case 'debug':
                console.log('[DEBUG] ', message, data); break;
            default:
                console.log('[LOG] ',message, data); break;
        }
    };

    this.clearLogs = function() {
        console.clear();
    };
}
