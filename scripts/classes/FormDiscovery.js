/**
 * Global variables:
 * - configObject
 */
function FormDiscovery() {

    "use strict";

    var _utility = null;

    var usernameRegex = /(user name|username|user|name|usr|email|mail|signin|signon|login)/ig;
    var passwordRegex = /(pass word|password|pass|pwd|signin|signon|login)/ig;
    var rememberRegex = /(Remember|rmb|login|signin|signon)/ig;
    var loginRegex = /(login|log in|sign in|signin|signon|sign\-in|submit)/ig;

    this.init = function(utility) {
        _utility = utility;
    };

    this.launchFormDiscovery = function() {
        return $.Deferred(function (dfd) {
            var formFields = [];

            var $passwords = $('body').find('input[type="password"]');
            if($passwords.length > 0) {
                var loginFormFields = {};

                findAllFormsInPage($passwords, formFields);
                formFields.forEach(function(item) {
                    if(item.form.formElement) {
                        // Handle if elements has in a form
                        findAllElementsInForm(item);
                    } else {
                        // Handle if elements isn't in any form
                        findAllElementsNotInForm(item);
                    }
                });

                evaluateFormsInPage(formFields);
                loginFormFields = evaluateLoginForm(formFields);

                console.log('loginFormFields', loginFormFields);

                dfd.resolve(loginFormFields);
            } else {
                dfd.reject('No login form has been found!');
            }
        }).promise();
    };

    this.populateCredential = function(formFields, credential) {
        var username = formFields.username;
        var password = formFields.password;
        var valueUsername = credential.username;
        var valuePassword = credential.password;

        $(username.selector).val(valueUsername).show();
        $(password.selector).val(valuePassword).show();
    }

    function findAllFormsInPage($passwords, formFields) {
        $passwords.each(function(index, element) {
            var $element = $(element)[0];
            var $form = $($element.form)[0];

            var formObject = {
                form: {
                    traceElement: $element,
                    formElement: $form,
                    weightNumber: 0,
                    textInput: 0,
                    passwordInput: 0,
                    elements: []
                }
            };

            $element.isInForm = $form ?  true : false;

            formFields.push(formObject);
        });
    }

    function findAllElementsInForm(item) {
        // Handle if elements has in a form
        var $form = $(item.form.formElement)[0];

        for(var i = 0, len = $form.length; i < len; i++)
            item.form.elements.push($form[i]);

        item.form.elements.forEach(function(element) {
            item.form.weightNumber = checkElementInput(element, item);
        });
    }

    function findAllElementsNotInForm(item) {
        // Handle if elements has in a form
        item.form.formElement = '*DUMMY_FORM*';

        getAllVisibleTextElements(item);
        getAllVisibleSubmitElements(item);
        getAllVisibleButtonElements(item);
        getAllVisibleAElements(item);
        var passwordElement = item.form.traceElement;

        item.form.elements.push(passwordElement);

        item.form.elements.forEach(function(element) {
            item.form.weightNumber = checkElementInput(element, item);
        });
    }

    function getAllVisibleTextElements(item) {
        $('body').find('input[type="text"], input[type="email"]').each(function() {
            var $element = $(this)[0];

            if($element.form) return;
            if(!$element.hidden) {
                item.form.elements.push($element);
            }
        });
    }

    function getAllVisibleSubmitElements(item) {
        $('body').find('input[type="submit"], input[type="button"]').each(function() {
            var $element = $(this)[0];

            if($element.form) return;
            if(!$element.hidden) {
                item.form.elements.push($element);
            }
        });
    }

    function getAllVisibleButtonElements(item) {
        $('body').find('button[type="submit"], button[type="button"]').each(function() {
            var $element = $(this)[0];

            if($element.form) return;
            if(!$element.hidden) {
                item.form.elements.push($element);
            }
        });
    }

    function getAllVisibleAElements(item) {
        $('body').find('a').each(function() {
            var $element = $(this)[0];
            var onClick = typeof $element.onclick;

            if ($element.form) return;
            if (!$element.hidden && (onClick === 'function' || onClick === 'boolean')) {
                item.form.elements.push($element);
            }
        })
    }

    function evaluateFormsInPage(formFields) {
        formFields.forEach(function(item, index) {
            if(item.form.formElement) {
                if(!item.form.formElement.offsetWidth && !item.form.formElement.offsetHeight) {
                    item.form.weightNumber = -1;
                }
            } else {
                item.form.weightNumber -= 2;
            }

            if(item.form.textInput > 1) {
                item.form.weightNumber -= 3;
            }
            if(item.form.passwordInput > 1) {
                item.form.weightNumber -= 3;
            }

            if(item.form.weightNumber < 0) {
                formFields.slice(index, 1);
            }
        });
    }

    function checkElementInput(element, formObject) {
        var $element = $(element)[0];
        var tagName = $element.tagName;
        var type = $element.type;
        var weightNumber = formObject.form.weightNumber;
        if(tagName === 'INPUT') {
            switch(type) {
                case 'text':
                case 'email':
                    formObject.form.textInput++;
                    weightNumber += evaluateTextInput(element); break;
                case 'password':
                    formObject.form.passwordInput++;
                    weightNumber += evaluatePasswordInput(element); break;

                // evaluate if it is a "remember me" checkbox or not
                case 'checkbox':
                    weightNumber += evaluateCheckboxInput(element); break;

                case 'submit':
                case 'button':
                // input[type=image]: Defines an image as the submit button, so we
                // should check it as a submit button
                case 'image':
                    weightNumber += evaluateSubmitInput(element); break;

                case 'radio':
                case 'file':
                case 'reset': weightNumber--; break;

                case 'hidden': break;

                default: break;
            }
        } else if(tagName === 'BUTTON') {
            switch(type) {
                case 'submit':
                case 'button':
                    weightNumber += evaluateSubmitInput(element); break;
                default: break;
            }
        } else if (tagName === 'A') { 
            var onClick = typeof $element.onclick;
            switch(onClick) {
                case 'function':
                case 'boolean':
                    weightNumber += evaluateSubmitInput(element); break;
                default: break;
            }
        }

        return weightNumber;
    }

    function evaluateTextInput(element) {
        var $element = $(element)[0];
        var weightElement = 0;

        if(usernameRegex.test($element.id)) {
            weightElement += 3;
        }

        if(usernameRegex.test($element.className)) {
            weightElement += 2;
        }

        if($element.type === 'text') {
            weightElement += 1;
        }

        if(usernameRegex.test($element.name)) {
            weightElement++;
        }

        if(usernameRegex.test($element.placeholder)) {
            weightElement++;
        }

        element.weightForUsernameInput = weightElement;
        return weightElement;
    }

    function evaluatePasswordInput(element) {
        var $element = $(element)[0];
        var weightElement = 0;

        if(passwordRegex.test($element.id)) {
            weightElement += 3;
        }

        if(passwordRegex.test($element.className)) {
            weightElement += 2;
        }

        if($element.type === 'password') {
            weightElement += 1;
        }

        if(passwordRegex.test($element.name)) {
            weightElement++;
        }

        if(passwordRegex.test($element.placeholder)) {
            weightElement++;
        }

        element.weightForPasswordInput = weightElement;
        return weightElement;
    }

    function evaluateCheckboxInput(element) {
        var $element = $(element)[0];
        var weightElement = 0;

        if(rememberRegex.test($element.id)) {
            weightElement++;
        }

        if(rememberRegex.test($element.className)) {
            weightElement++;
        }

        if(rememberRegex.test($element.name)) {
            weightElement++;
        }

        return weightElement;
    }

    function evaluateSubmitInput(element) {
        var $element = $(element)[0];
        var weightElement = 0;

        if(loginRegex.test($element.id)) {
            weightElement += 3;
        }

        if(loginRegex.test($element.className)) {
            weightElement += 2;
        }

        if($element.type === 'button' || $element.type === 'submit') {
            weightElement += 1;
        }

        if(loginRegex.test($element.name)) {
            weightElement++;
        }

        if(loginRegex.test($element.value)) {
            weightElement++;
        }

        if(loginRegex.test($element.innerText)) {
            weightElement++;
        }

        if(typeof $element.onclick === 'function' || typeof $element.onsubmit === 'function') {
            weightElement++;
        }

        element.weightForSubmit = weightElement;
        return weightElement;
    }

    function evaluateLoginForm(formFields) {
        var loginFormFields = {};
        var loginForm = formFields.reduce(function(total, item) {
            return item.form.weightNumber >= total.form.weightNumber ? item : total;
        });

        loginFormFields.username = setDataForUsernameInput(loginForm);
        loginFormFields.password = setDataForPasswordInput(loginForm);
        loginFormFields.submit = setDataForSubmitButton(loginForm);
        loginFormFields.isTraining = true;

        return loginFormFields;
    }

    function setDataForUsernameInput(loginForm) {
        var usernameField = findElementWithHighestWeight(loginForm, 'weightForUsernameInput');

        var $username = $(usernameField)[0];
        var selectorString = getElementSelector(usernameField);
        var xpathString = getElementXpath(usernameField);

        return {
            element: $username,
            selector: selectorString,
            xpath: xpathString,
            attrs: $username ? createElementAttributes($username) : null
        };
    }

    function setDataForPasswordInput(loginForm) {
        var passwordField = findElementWithHighestWeight(loginForm, 'weightForPasswordInput');

        var $password = $(passwordField)[0];
        var selectorString = getElementSelector(passwordField);
        var xpathString = getElementXpath(passwordField);
    
        return {
            element: $password,
            selector: selectorString,
            xpath: xpathString,
            attrs: $password ? createElementAttributes($password) : null
        };
    }

    function setDataForSubmitButton(loginForm) {
        var submitButton = findElementWithHighestWeight(loginForm, 'weightForSubmit');
        var $submitButton = $(submitButton)[0];
        var selectorString = getElementSelector(submitButton);
        var xpathString = getElementXpath(submitButton);

        return {
            element: $submitButton,
            selector: selectorString,
            xpath: xpathString,
            attrs: $submitButton ? createElementAttributes($submitButton) : null
        };
    }

    function findElementWithHighestWeight(loginForm, key) {
        var maxValue = 0;
        var objectIndex = null;
        loginForm.form.elements.forEach(function(item, index) {
            if(item[key] !== null && item[key] !== undefined) {  
                if(item[key] > maxValue) {
                    maxValue = item[key];
                    objectIndex = index;
                }
            }
        });

        return loginForm.form.elements[objectIndex];
    }

    function createElementAttributes($element) {
        var attribute = {
            tagName: $element.tagName,
            id: $element.id,
            className: $element.className,
            type: $element.type,
            placeholder: $element.placeholder,
            disabled: $element.disabled,
            readOnly: $element.readOnly,
            required: $element.required,
            hidden: $element.hidden,
            //isVisible: $element.style.display === 'none' ? false : true
        };

        return attribute;
    }

    function getElementSelector(element) {
        var pieces = [];

        for (; element && element.tagName !== undefined; element = element.parentNode) {
            if (element.className) {
                var classes = element.className.split(' ');
                for (var i in classes) {
                    if (classes.hasOwnProperty(i) && classes[i]) {
                        pieces.unshift(classes[i]);
                        pieces.unshift('.');
                    }
                }
            }
            if (element.id && !/\s/.test(element.id)) {
                pieces.unshift(element.id);
                pieces.unshift('#');
            }
            pieces.unshift(element.tagName);
            pieces.unshift(' > ');
        }

        return pieces.slice(1).join('');
    };

    function getElementXpath(elm) {
        var allNodes = document.getElementsByTagName('*');
        for (var segs = []; elm && elm.nodeType == 1; elm = elm.parentNode)
        {
            if (elm.hasAttribute('id')) {
                var uniqueIdCount = 0;
                for (var n = 0; n < allNodes.length ;n++) {
                    if (allNodes[n].hasAttribute('id') && allNodes[n].id == elm.id) uniqueIdCount++;
                    if (uniqueIdCount > 1) break;
                }
                if ( uniqueIdCount == 1) {
                    segs.unshift('id("' + elm.getAttribute('id') + '")');
                    return segs.join('/');
                } else {
                    segs.unshift(elm.localName.toLowerCase() + '[@id="' + elm.getAttribute('id') + '"]');
                }
            } else if (elm.hasAttribute('class')) {
                segs.unshift(elm.localName.toLowerCase() + '[@class="' + elm.getAttribute('class') + '"]');
            } else {
                for (var i = 1, sib = elm.previousSibling; sib; sib = sib.previousSibling) {
                    if (sib.localName == elm.localName)
                        i++;
                }
                segs.unshift(elm.localName.toLowerCase() + '[' + i + ']');
            };
        };
        return segs.length ? '/' + segs.join('/') : null;
    };



    this.lookupElementByXPath = function(path) {
        var evaluator = new XPathEvaluator();
        var result = evaluator.evaluate(path, document.documentElement, null,XPathResult.FIRST_ORDERED_NODE_TYPE, null);
        return  result.singleNodeValue;
    };
}