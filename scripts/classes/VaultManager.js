/**
 * Global variables:
 * - configObject
 */
function VaultManager() {

    'use strict';

    var _vault = null;
    var _utility = null;

    this.init = function(utility) {
        _vault = createVaultArchive();
        _utility = utility;
    };

    this.getVault = function() {
        return _vault;
    };

    function createVaultArchive() {
        return {
            config: {
                storedInLocalStorage: false,
            },
            pages: []
        };
    }

    this.createSingleVault = function(data, url) {
        if(!checkPageIsInVault(url)) {
            createSingleVault(data.credential, url);
        }
    };

    this.updateFormFields = function(data, url) {
        if(checkPageIsInVault(url)) {
            updateSingleVault(data.formFields, url);
        }
    };

    function checkPageIsInVault(url) {
        var isExistingInVault = false;
        var location = _utility.getLocation(url);
        if(_vault.pages.length) {
            _vault.pages.forEach(function(item) {
                if(item.location.host === location.host) {
                    isExistingInVault = true;
                }
            });
        }
        return isExistingInVault;
    }

    function createSingleVault(credential, url) {
        var location = _utility.getLocation(url);
        var singleVault = {
            url: url,
            location: location,
            credential: credential,
            formFields: {}
        };

        _vault.pages.push(singleVault);
    }

    function updateSingleVault(formFields, url) {
        var location = _utility.getLocation(url);
        var index = _vault.pages.findIndex(function(item) {
            return item.location.host === location.host;
        });

        _vault.pages[index].formFields = formFields;
    }

    this.loadVault = function(url) {
        return $.Deferred(function (dfd) {
            var vault = null;
            if(_vault.pages.length) {
                _vault.pages.forEach(function(page) {
                    var urlObject = _utility.getLocation(url);

                    if(urlObject.host === page.location.host) {
                        vault = page;
                    }
                });
            }
            dfd.resolve(vault);
        }).promise();
    };
}
