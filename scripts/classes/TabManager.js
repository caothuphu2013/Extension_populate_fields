/**
 * Global variables:
 * - configObject
 */
function TabManager() {

    'use strict';

    var _tabs = null;

    this.init = function() {
        _tabs = [];
    };

    this.getAllTabs = function() {
        return _tabs;
    };

    this.createNewTab = function(tab) {
        if(!this.checkTabIsExisting(tab.id)) {
            var _tab = {
                id: tab.id,
                title: tab.title,
                url: tab.url
            };
            _tabs.push(_tab);
        }
    };

    this.updateTab = function(tab) {
        var index = _tabs.findIndex(function(item) {
            return item.id === tab.id;
        });

        _tabs[index].title = tab.title;
        _tabs[index].url = tab.url;
    };

    this.checkTabIsExisting = function(tabId) {
        var isExistingTab = false;
        _tabs.forEach(function(item) {
            if(item.id === tabId) {
                isExistingTab = true;
            }
        });
        return isExistingTab;
    };
}
