/**
 * Global variables:
 * - configObject
 */
function Account() {

    'use strict';

    var _account = null;

    this.init = function() {
        _account = createAccountArchive();
    };

    function createAccountArchive() {
        return {
            accessToken: null
        };
    }

    this.getAccountCredencial = function() {
        return _account;
    };

    this.findAccessTokenFromDomain = function() {
        return $.Deferred(function (dfd) {
            chrome.cookies.getAll({domain: configObject.domainURL}, function(cookies) {
                //TODO: Should change cookie key for client domain
                // var accessToken = getCookieValue(cookies, 'TIKI_ACCESS_TOKEN');
                var accessToken = 'FAKE_ACCESS_TOKEN_FOR_APIS';
                if(accessToken) {
                    dfd.resolve(accessToken);
                } else {
                    dfd.reject();
                }
            });

        }).promise();
    };

    function getCookieValue(cookies, key) {
        if(cookies.length) {
            for(var i = 0, len = cookies.length; i < len; i++) {
                if(cookies[i].name === key) {
                    return cookies[i].value;
                }
            }
        }
    }

    this.setAccessToken = function(accessToken) {
        _account.accessToken = accessToken;
    };
}
