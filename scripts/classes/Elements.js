// var Elements_Class = (function($){
//     "use strict";

//     var instance;

//     function init() {
//        return {
//            setForm: _setForm,
//            setElement: _setElement
//        }
//     }

//     return {
//         getInstance: function() {
//             if (!instance) {
//                 instance = init();
//             }
//             return instance;
//         }
//     }

//     function _getID(element) {
//         return $(element).attr('id') || '';
//     }

//     function _getName(element) {
//         return $(element).attr('name') || '';
//     }

//     function _getType(element) {
//         return $(element).attr('type') || 'text';
//     }

//     function _getClass(element) {
//         return $(element).attr('class') || '';
//     }

//     function _getPlaceHolder(element) {
//         return $(element).attr('placeholder') || '';
//     }

//     function _getValue(element) {
//         return $(element).val() || '';
//     }

//     function _getTagName(element) {
//         return $(element).nodeName || '';
//     }

//     function _getVisible(element) {
//         return $(element).is(':visible');
//     }

//     function _getAction(element) {
//         return $(element).attr('action');
//     }

//     function _getMethod(element) {
//         return $(element).attr('method');
//     }

//     function _getWeight(element, arrIdentification) {
//         var name = _getName(element);
//         var id = _getID(element);
//         var placeholder = _getPlaceHolder(element);
//         var classElement = _getClass(element);

//         var arrInfo = [name, id, placeholder, classElement];
//         var lengthInfo = arrInfo.length;
//         var lengthIdentification = arrIdentification.length;

//         var weight = 0;
//         for (let i = 0;i < lengthInfo;i++) {
//             var info_item = arrInfo[i].toLowerCase();
//             for (let j = 0;j < lengthIdentification;j++) {
//                 var identification_item = arrIdentification[j].toLowerCase();
//                 if (info_item.includes(identification_item)) {
//                     weight++;
//                     break;
//                 }
//             }
//         }

//         return weight;
//     }

//     function _getWeightForm(xpathUsername, xpathPassword, xpathSubmit) {
//         var Utility = Utility_Class.getInstance();

//         var username = Utility.getElementByXpath(xpathUsername);
//         var password = Utility.getElementByXpath(xpathPassword);
//         var submit = Utility.getElementByXpath(xpathSubmit);

//         var type_username = _getType(username);
//         var type_password = _getType(password);
//         var type_submit = _getType(submit);

//         var weight_username = _getWeight(username, type_username);
//         var weight_password = _getWeight(password, type_password);
//         var weight_submit = _getWeight(submit, type_submit);

//         return weight_username + weight_password + weight_submit;
//     }


//     function _setElement(element) {
//         var Utility = Utility_Class.getInstance();

//         var xpath = Utility.getXpath(element);
//         var id = _getID(element);
//         var name = _getName(element);
//         var type = _getType(element);
//         var classElement = _getClass(element);
//         var placeholder = _getPlaceHolder(element);
//         var value = _getValue(element);
//         var tagName = _getTagName(element);
//         var visible = _getVisible(element);

//         var arrIdentification = Utility.getArrIdentification(type);
//         var weight = _getWeight(element, arrIdentification);

//         return {
//             xpath,
//             element,
//             id,
//             name,
//             type,
//             classElement,
//             placeholder,
//             value,
//             tagName,
//             visible,
//             weight
//         }

//     }

//     function _setForm(element, xpathForm, xpathUsername, xpathPassword, xpathSubmit, weightDefault = 0) {
//         var id = _getID(element);
//         var name = _getName(element);
//         var action = _getAction(element);
//         var method = _getMethod(element);
//         var weight = weightDefault + _getWeightForm(xpathUsername, xpathPassword, xpathSubmit);

//         return {
//             xpathForm,
//             xpathUsername,
//             xpathPassword,
//             xpathSubmit,
//             element,
//             id,
//             name,
//             action,
//             method,
//             weight
//         }
//     }
// })($);