/**
 * Global variables:
 * - configObject
 */
(function($){
    'use strict';

    var extensionConfig = {};

    var utility = new Utility();
    var debug = new Debug();

    var apiCaller = new ApiCaller();
    var account = new Account();
    var tabManager = new TabManager();
    var vaultManager = new VaultManager();

    $(document).ready(function() {
        setupExtensionEnvironment();
        prepareDataForExtension();
        initializeBackground();
    });

    function setupExtensionEnvironment() {
        account.init();
        tabManager.init();
        vaultManager.init(utility);
    }

    function prepareDataForExtension() {
        // Default is true;
        getStatusFromStorage(true);
        getAccessToken();
    }

    function initializeBackground() {
        debug.log('Bootstrap background.js sucessfully!');
        chrome.extension.onMessage.addListener(function(request, sender) {
            debug.log('request', request, configObject.logType.debug);
            debug.log('sender', sender, configObject.logType.debug);
            switch(request.message) {
                case 'evaluateVault':
                    evaluateVault(sender);
                    break;

                case 'createNewSingleVault':
                    vaultManager.createSingleVault(request.data, sender.url);
                    chrome.tabs.sendMessage(sender.tab.id, { message: 'launchFormDiscovery' });
                    break;
                case 'updateFormFields':
                    vaultManager.updateFormFields(request.data, sender.url);
                    debug.log('All Vaults: ', vaultManager.getVault(), configObject.logType.debug);
                    evaluateVault(sender);
                    break;

                case 'activeExtension':
                    debug.log('Extension is activated', null);
                    setStatusFromStorage(true);
                    break;
                case 'deactivateExtension':
                    debug.log('Extension is deactivated', null);
                    setStatusFromStorage(false);
                    break;

                case 'flashExtensionIcon':
                    debug.log('Flash icon', null);
                    break;

                case 'endProcess':
                    debug.log('=> Populated credential successfully !!!', null);
                    break;
                default: break;
            }
        });

        chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
            if (changeInfo.status == 'complete') {
                debug.log('Trigger on page loaded', null, configObject.logType.debug);

                if(!account.getAccountCredencial().accessToken) {
                    getAccessToken();
                } else {
                    if(!tabManager.checkTabIsExisting(tabId)) {
                        tabManager.createNewTab(tab);
                    } else {
                        tabManager.updateTab(tab);
                    }

                    var regex = /chrome:\/\/(.)*/;
                    if(extensionConfig.isActivate && !regex.test(tab.url)) {
                        debug.log('Inject content script', null, configObject.logType.debug);
                        injectScript(tabId);
                    }
                    debug.log('Tabs: ', tabManager.getAllTabs(tab), configObject.logType.debug);
                }
            }
        });
    }

    function injectScript(onTabId) {
        try {
            var tabId = parseInt(onTabId);
            chrome.tabs.executeScript(tabId, {
                file: configObject.path.extensionPath
            });
        } catch(error) {
            debug.log(null, error, configObject.logType.error);
        }
    }

    function getAccessToken() {
        $.when(account.findAccessTokenFromDomain()).then(function(accessToken) {
            account.setAccessToken(accessToken);
            debug.log('Access Token has been found: ', account.getAccountCredencial().accessToken);
        }, function() {
            debug.log('No access token has been found!', null);
        });
    }

    function evaluateVault(sender) {
        $.when(vaultManager.loadVault(sender.url)).then(function(vault) {
            if(vault) {
                if(vault.formFields.isTraining) {
                    chrome.tabs.sendMessage(sender.tab.id, { message: 'populateCredential', data: vault });
                } else {
                    chrome.tabs.sendMessage(sender.tab.id, { message: 'launchFormDiscovery' });
                }
            } else {
                chrome.tabs.sendMessage(sender.tab.id, { message: 'requestPageInfo' });
            }

        }, function(error) {
            debug.log(null, error, configObject.logType.error);
        });
    }

    function getStatusFromStorage(defaultStatus) {
        chrome.storage.sync.get({
            isActivate: defaultStatus
        }, function(items) {
            extensionConfig.isActivate = items.isActivate;
        });
    }

    function setStatusFromStorage(status) {
        chrome.storage.sync.set({
            isActivate: status
        }, function() {
            extensionConfig.isActivate = status;
        })
    }

})($);
