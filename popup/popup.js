/**
 * Global variables:
 * - configObject
 */
(function($){
    'use strict';

    document.addEventListener('DOMContentLoaded', function(){    
        init();
    });

    function init() {
        chrome.storage.sync.get({
            statusToggle: true
        }, function(items) {
            var statusToggle = items.statusToggle;

            sendMessageStatusToggle(statusToggle);

            setStatusToggleButton('#toggle', statusToggle);
            changeStatusToggle();    
        });
    }

    function changeStatusToggle() {
        $('#toggle').on('change', function() {
            var statusToggle = $('#toggle').prop('checked');
            
            sendMessageStatusToggle(statusToggle);
            chrome.storage.sync.set({
                statusToggle
            })
    
        });
    }

    function sendMessageStatusToggle(statusToggle) {
        if (statusToggle) {
            sendMessage('activeExtension');
        }
        else {
            sendMessage('deactivateExtension');
        }
    }

    function setStatusToggleButton(seletor, statusToggle) {
        $(seletor).prop('checked', statusToggle);        
    }

    function sendMessage(message) {
        chrome.runtime.sendMessage({
            message: message
        });
    }

})($);
